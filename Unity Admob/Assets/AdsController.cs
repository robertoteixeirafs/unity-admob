﻿using UnityEngine;
using System.Collections;

public class AdsController : MonoBehaviour 
{
    public string PublisherId = "";

    public void ExibirAnuncio()
    {
        GoogleMobileAdsPlugin.CreateInterstitial(PublisherId);
        GoogleMobileAdsPlugin.RequestInterstitial(true);
    }
    

}
